const express = require('express')
const db =require('./db')
const util= require('./utils')

const router = express.Router()

router.get('/displaymovie',(request, response)=>{

    console.log(request.body)


const statement = `SELECT 
                    movie_name , movie_release_date, movie_time, director from Movie
                    `

db.pool.query(statement,(error , data)=>{

    response.send(util.createResult(error , data))
})
     
})

router.post('/addmovie',(request, response)=>{

    console.log(request.body)
const {movie_name ,movie_release_date , movie_time , director}= request.body

const statement = `INSERT into Movie (movie_name , movie_release_date, movie_time, director) 
                     values (?,?,?,?)`

db.pool.query(statement ,[movie_name,movie_release_date,movie_time,director],(error , data)=>{

    response.send(util.createResult(error , data))
})
     
})

router.put('/updatemovie',(request, response)=>{

    
    console.log(request.body)
    
    
const {movie_name,movie_release_date, movie_time, director}= request.body

const statement = `UPDATE movie SET   movie_release_date =? , movie_time=? , director =?
                      
                    WHERE movie_name =?`

db.pool.query(statement ,[  movie_release_date,movie_time,director , movie_name],(error , data)=>{

    response.send(util.createResult(error , data))
})
     
})

router.delete('/deletemovie',(request, response)=>{

    console.log(request.body)
const {movie_name,director}= request.body

const statement = `DELETE from Movie
                    WHERE movie_name =? and director =?`

db.pool.query(statement ,[movie_name,director],(error , data)=>{

    response.send(util.createResult(error , data))
})
     
})

module.exports=router