const express = require('express')

const app = express()

app.use(express.json())

const movieRouter = require('./movie')

app.use(movieRouter)

app.listen(4000, '0.0.0.0',()=>{
    console.log('server started at port 4000')
})